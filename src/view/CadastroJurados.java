package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import dao.*;
import pojo.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CadastroJurados extends JFrame {

	private JPanel contentPane;
	private JTextField txtCodigo;
	private JTextField txtNome;
	private JButton btnApagar;
	private JButton btnEditar;
	private JButton btnInserir;
	private JRadioButton rdAtualizar;
	private JRadioButton rdPesquisar;
	private JRadioButton rdCadastrar;
	private JRadioButton rdExcluir;
	private JMenu menuPesquisar;
	private JTable table;
	private JComboBox cbQuesito;
	private JButton btnAdd;
	private DefaultTableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroJurados frame = new CadastroJurados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroJurados() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
			}
		});
		setTitle("Cadastro de Jurados");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 469, 569);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuPesquisar = new JMenu("Pesquisar");
		menuBar.add(menuPesquisar);
		menuPesquisar.setVisible(false);
		
		
		JMenuItem mntmPorCdigo = new JMenuItem("Por C\u00F3digo");
		mntmPorCdigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Pesquisar();
			}
		});
		menuPesquisar.add(mntmPorCdigo);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Por Nome");
		menuPesquisar.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCdigoJurado = new JLabel("C\u00F3digo Jurado");
		lblCdigoJurado.setBounds(27, 74, 84, 14);
		contentPane.add(lblCdigoJurado);
		
		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(118, 68, 86, 20);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(27, 117, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setEditable(false);
		txtNome.setBounds(72, 114, 132, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		rdCadastrar = new JRadioButton("Cadastrar");
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastrar();
			}
		});
		rdCadastrar.setBounds(287, 27, 109, 23);
		contentPane.add(rdCadastrar);
		
		rdExcluir = new JRadioButton("Excluir");
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluir();
			}
		});
		rdExcluir.setBounds(287, 70, 109, 23);
		contentPane.add(rdExcluir);
		
		rdAtualizar = new JRadioButton("Atualizar");
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilAtualizar();
			}
		});
		rdAtualizar.setBounds(287, 113, 109, 23);
		contentPane.add(rdAtualizar);
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Inserir();
			}
		});
		btnInserir.setBounds(27, 213, 89, 23);
		btnInserir.setVisible(false);
		
		contentPane.add(btnInserir);
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilPesquisar();
			}
		});
		rdPesquisar.setBounds(287, 159, 109, 23);
		contentPane.add(rdPesquisar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Atualizar();
			}
		});
		btnEditar.setBounds(137, 213, 89, 23);
		contentPane.add(btnEditar);
		btnEditar.setVisible(false);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Excluir();
			}
		});
		btnApagar.setBounds(244, 213, 89, 23);
		btnApagar.setVisible(false);
		
		contentPane.add(btnApagar);
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
		
		cbQuesito = new JComboBox();
		cbQuesito.setEnabled(false);
		cbQuesito.setBounds(137, 277, 212, 20);
		contentPane.add(cbQuesito);
		
		JLabel lblListaDeQuesitos = new JLabel("Lista de Quesitos");
		lblListaDeQuesitos.setBounds(27, 280, 100, 14);
		contentPane.add(lblListaDeQuesitos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(39, 316, 401, 174);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id Jurado", "Id Quesito", "Nome Quesito", "N\u00BA Jurado"
			}
		);
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setPreferredWidth(111);
		table.getColumnModel().getColumn(1).setPreferredWidth(98);
		table.getColumnModel().getColumn(2).setPreferredWidth(232);
		table.getColumnModel().getColumn(3).setPreferredWidth(112);
		
		btnAdd = new JButton("ADD");
		btnAdd.setEnabled(false);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InserirJuradoQuesito();
				atualizaTabela();
			}
		});
		btnAdd.setBounds(359, 276, 89, 23);
		contentPane.add(btnAdd);
	}
	
	public void perfilCadastrar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(true);
		btnInserir.setVisible(true);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
		cbQuesito.setEnabled(true);
		btnAdd.setEnabled(false);
		modelo.setRowCount(0);
		preencheComboQuesito();
		
	}
	
	public void perfilAtualizar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(true);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(true);
		menuPesquisar.setVisible(false);
		cbQuesito.setEnabled(true);
		preencheComboQuesito();
		btnAdd.setEnabled(true);
	}
	
	public void perfilExcluir(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(true);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
		cbQuesito.setSelectedIndex(-1);
		cbQuesito.setEnabled(false);
		btnAdd.setEnabled(false);
		
	}
	

	public void perfilPesquisar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(true);
		cbQuesito.setSelectedIndex(-1);
		cbQuesito.setEnabled(false);
		btnAdd.setEnabled(false);
	}
	
	
	
	public void Inserir(){
			Jurado jurado=new Jurado();
			jurado.setNome(txtNome.getText());
			
			JuradosDao daojurado=new JuradosDao();
			daojurado.insereJurado(jurado);
			apagar();
			
			
	}
	
	public void InserirJuradoQuesito(){
		
		if(cbQuesito.getSelectedItem().toString().isEmpty()){
			JOptionPane.showMessageDialog(null, "N�o foi possivel cadastrar quesito para jurado");
		}
		else{
			JuradoQuesito jurado=new JuradoQuesito();
			jurado.setNome(txtNome.getText());
			jurado.setId_jurado(Integer.parseInt(txtCodigo.getText()));
			String texto=cbQuesito.getSelectedItem().toString();
			String codigo=texto.substring(0,texto.lastIndexOf("-"));
			jurado.setId_quesito(Integer.parseInt(codigo));
			
			JuradosQuesitoDao daojurado=new JuradosQuesitoDao();
			daojurado.insereJuradoQuesito(jurado);
			atualizaTabela();
			apagar();
		}
	}
	
	
	
	public void Excluir(){
		Jurado jurado=new Jurado();
		jurado.setId_jurado(Integer.parseInt(txtCodigo.getText()));
		
		JuradosDao daojurado=new JuradosDao();
		daojurado.excluiJurado(jurado);
		
		
		JuradoQuesito jurado2=new JuradoQuesito();
		jurado2.setId_jurado(Integer.parseInt(txtCodigo.getText()));
		
		JuradosQuesitoDao daojuradosquesito=new JuradosQuesitoDao();
		daojuradosquesito.excluiJuradoQuesito(jurado2);
		atualizaTabela();
		apagar();
	}
	
	
	public void Atualizar(){
		Jurado jurado=new Jurado();
		jurado.setId_jurado(Integer.parseInt(txtCodigo.getText()));
		jurado.setNome(txtNome.getText());
		
		JuradosDao daojurado=new JuradosDao();
		daojurado.atualizaJurado(jurado);
		atualizaTabela();
		apagar();
	}
	
	public void Pesquisar(){
		try{
			Jurado jurado=new Jurado();
			jurado.setId_jurado(Integer.parseInt(JOptionPane.showInputDialog("Digite o c�digo do jurado")));
				
			JuradosDao daojurado=new JuradosDao();
			setJurados(daojurado.consultaJurado(jurado));
			atualizaTabela();
		}
		catch(NumberFormatException erro){
			JOptionPane.showMessageDialog(null, "O c�digo digitado � inv�lido");
		}
	}
	
	
	
	public void setJurados(Jurado jurado){
		txtCodigo.setText(String.valueOf(jurado.getId_jurado()));
		txtNome.setText(jurado.getNome());
		
	}
	
	public void apagar(){
		txtCodigo.setText("");
		txtNome.setText("");
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
		modelo.setRowCount(0);
		cbQuesito.setSelectedIndex(-1);
		cbQuesito.setEnabled(false);
		btnAdd.setEnabled(false);
		
	}
	
	
	
	public List<Quesito> listaQuesitos(){
		QuesitoDao pDao=new QuesitoDao();
		List<Quesito> lista=new ArrayList<Quesito>();
		lista=pDao.consultaListaQuesito();
		return lista;
	}
	
	public List<JuradoQuesito> listaQuesitosJurados(){
		JuradosQuesitoDao pDao=new JuradosQuesitoDao();
		
		JuradoQuesito jq=new JuradoQuesito();
		jq.setId_jurado(Integer.parseInt(txtCodigo.getText()));
		
		List<JuradoQuesito> lista=new ArrayList<JuradoQuesito>();
		lista=pDao.consultaListaJuradoQuesito2(jq);
		return lista;
	}
	
	
	public void preencheComboQuesito(){
		if(cbQuesito.getItemCount()!=0){
			cbQuesito.removeAllItems();
		}
		List<Quesito> lista=listaQuesitos();
			if(lista!=null){
				for(Quesito p:lista){
					cbQuesito.addItem(p.getId_quesito()+"-"+p.getNome());
					
				}
			}
		
	}
	
	

	public void atualizaTabela(){
		List<JuradoQuesito> lista=listaQuesitosJurados();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(JuradoQuesito p:lista){
				Object[] objeto=new Object[4];
				objeto[0]=p.getId_jurado();
				objeto[1]=p.getId_quesito();
				objeto[2]=p.getNome();
				objeto[3]=p.getNumeroJurado();
				modelo.addRow(objeto);
				
			}
		}
		
	}
	
}


