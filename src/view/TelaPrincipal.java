package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.SwingConstants;

public class TelaPrincipal extends JFrame {
	private CadastroEscola ce;
	private CadastroJurados cj;
	private CadastroQuesitos cq;
	private ViewApuracao va;
	private TelaVerQuesito tvq;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				ce=new CadastroEscola();
				cj=new CadastroJurados();
				cq=new CadastroQuesitos();
				va=new ViewApuracao();
				tvq=new TelaVerQuesito();
				
			}
		});
		setTitle("Sistema de Apura\u00E7\u00E3o de Escola de Samba");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 301, 257);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCadastrar = new JMenu("Cadastrar");
		menuBar.add(mnCadastrar);
		
		JMenuItem mntmEscolas = new JMenuItem("Escolas");
		mntmEscolas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					ce.show();
			}
		});
		mnCadastrar.add(mntmEscolas);
		
		JMenuItem mntmQuesitos = new JMenuItem("Quesitos");
		mntmQuesitos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cq.show();
			}
		});
		mnCadastrar.add(mntmQuesitos);
		
		JMenuItem mntmJurados = new JMenuItem("Jurados");
		mntmJurados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cj.show();
			}
		});
		mnCadastrar.add(mntmJurados);
		
		JMenu mnPesquisar = new JMenu("Pesquisar");
		menuBar.add(mnPesquisar);
		
		JMenuItem mntmVerQuesito = new JMenuItem("Ver Quesito");
		mntmVerQuesito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tvq.show();
			}
		});
		mnPesquisar.add(mntmVerQuesito);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnApuraoDeNotas = new JButton("Apura\u00E7\u00E3o de Notas");
		btnApuraoDeNotas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				va.show();
			}
		});
		btnApuraoDeNotas.setBounds(40, 44, 177, 23);
		contentPane.add(btnApuraoDeNotas);
	}
}
