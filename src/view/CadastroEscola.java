package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import pojo.*;
import dao.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class CadastroEscola extends JFrame {

	private JPanel contentPane;
	private JTextField txtCodigo;
	private JTextField txtNome;
	private JButton btnInserir;
	private JButton btnEditar;
	private JButton btnApagar;
	private JRadioButton rdCadastrar;
	private JRadioButton rdPesquisar;
	private JRadioButton rdAtualizar;
	private JRadioButton rdExcluir;
	private JMenu menuPesquisar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroEscola frame = new CadastroEscola();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroEscola() {
		setTitle("Cadastro de Escola");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuPesquisar = new JMenu("Pesquisar");
		menuBar.add(menuPesquisar);
		menuPesquisar.setVisible(false);
		
		JMenuItem mntmPorCdigo = new JMenuItem("Por C\u00F3digo");
		mntmPorCdigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Pesquisar();
			}
		});
		menuPesquisar.add(mntmPorCdigo);
		
		JMenuItem mntmPorNome = new JMenuItem("Por Nome");
		menuPesquisar.add(mntmPorNome);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel("C\u00F3digo");
		lblNome.setBounds(38, 33, 46, 14);
		contentPane.add(lblNome);
		
		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(90, 30, 129, 20);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JLabel lblNome_1 = new JLabel("Nome");
		lblNome_1.setBounds(38, 83, 46, 14);
		contentPane.add(lblNome_1);
		
		txtNome = new JTextField();
		txtNome.setEditable(false);
		txtNome.setBounds(90, 80, 139, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Cadastrar();
				
			}
		});
		btnInserir.setBounds(37, 211, 89, 23);
		contentPane.add(btnInserir);
		btnInserir.setVisible(false);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Atualizar();
				
			}
		});
		btnEditar.setBounds(155, 211, 89, 23);
		contentPane.add(btnEditar);
		btnEditar.setVisible(false);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Excluir();
				
			}
		});
		btnApagar.setBounds(274, 211, 89, 23);
		contentPane.add(btnApagar);
		btnApagar.setVisible(false);
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.setBounds(305, 99, 99, 23);
		contentPane.add(rdPesquisar);
		
		rdAtualizar = new JRadioButton("Atualizar");
		rdAtualizar.setBounds(305, 141, 99, 23);
		contentPane.add(rdAtualizar);
		
		rdExcluir = new JRadioButton("Excluir");
		rdExcluir.setBounds(305, 7, 89, 23);
		contentPane.add(rdExcluir);
		
		rdCadastrar = new JRadioButton("Cadastrar");
		rdCadastrar.setBounds(305, 52, 89, 23);
		contentPane.add(rdCadastrar);
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastrar();
				
			}
		});
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluir();
				
			}
		});
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilAtualizar();
			}
		});
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilPesquisar();
			}
		});
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
	}
	
	
	public void Cadastrar(){
		Escola escola=new Escola();
		escola.setNome(txtNome.getText());
		
		EscolaDao daoesc=new EscolaDao();
		daoesc.insereEscola(escola);
		perfilPadrao();
		
	}
	
	
	public void Excluir(){
		Escola escola=new Escola();
		escola.setId_escola(Integer.parseInt(txtCodigo.getText()));
		
		
		EscolaDao daoesc=new EscolaDao();
		daoesc.excluiEscola(escola);
		perfilPadrao();
	}
	
	public void Atualizar(){
		Escola escola=new Escola();
		escola.setId_escola(Integer.parseInt(txtCodigo.getText()));
		escola.setNome(txtNome.getText());
		
		EscolaDao daoesc=new EscolaDao();
		daoesc.atualizaEscola(escola);
		perfilPadrao();
	}
	
	
	public void Pesquisar(){
		try{
			Escola escola=new Escola();
			escola.setId_escola(Integer.parseInt(JOptionPane.showInputDialog("Digite o c�digo da escola")));
					
			EscolaDao daoesc=new EscolaDao();
			setEscola(daoesc.consultaEscola(escola));
		}
		catch(NumberFormatException erro){
			JOptionPane.showMessageDialog(null, "O c�digo digitado � inv�lido");
		}
		
		
	}
	
	public void perfilCadastrar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(true);
		btnInserir.setVisible(true);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
		
	}
	
	public void perfilAtualizar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(true);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(true);
		menuPesquisar.setVisible(false);
		
	}
	
	public void perfilExcluir(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(true);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
	}
	

	public void perfilPesquisar(){
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(true);
	}
	
	
	public void setEscola(Escola escola){
		txtCodigo.setText(String.valueOf(escola.getId_escola()));
		txtNome.setText(escola.getNome());
		
	}
	
	public void perfilPadrao(){
		txtCodigo.setText("");
		txtNome.setText("");
		txtCodigo.setEditable(false);
		txtNome.setEditable(false);
		btnInserir.setVisible(false);
		btnApagar.setVisible(false);
		btnEditar.setVisible(false);
		menuPesquisar.setVisible(false);
	}
}
