package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import pojo.*;
import dao.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class TelaVerQuesito extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox cbEscola;
	private DefaultTableModel modelo;
	private JRadioButton rdEscola;
	private JRadioButton rdQuesito;
	private JComboBox cbQuesito;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaVerQuesito frame = new TelaVerQuesito();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaVerQuesito() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				preencheComboEscola();
				 preencheComboQuesito();
				
			
			}
		});
		setTitle("Ver Quesito");
		setBounds(100, 100, 1060, 435);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 90, 1007, 260);
		contentPane.add(scrollPane);
		
		table = new JTable();
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nome Escola", "Quesito", "Nota 1", "Nota 2", "Nota 3", "Nota 4", "Nota 5", "Nota Maior", "Nota Menor", "Nota Total Quesito"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, true, true, true, true, true, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		};
		
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setPreferredWidth(81);
		table.getColumnModel().getColumn(9).setPreferredWidth(121);
		scrollPane.setViewportView(table);
		
		rdEscola = new JRadioButton("Escola");
		rdEscola.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cbEscola.setVisible(true);
				cbQuesito.setVisible(false);
				
				
			}
		});
		
		
		
		rdEscola.setBounds(40, 45, 109, 23);
		contentPane.add(rdEscola);
		
		rdQuesito = new JRadioButton("Quesito");
		rdQuesito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cbQuesito.setVisible(true);
				cbEscola.setVisible(false);
			
				
			}
		});
	
		
		rdQuesito.setBounds(496, 45, 109, 23);
		contentPane.add(rdQuesito);
		
		
		cbEscola = new JComboBox();
		cbEscola.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ResultadoApuracaoDao rad=new ResultadoApuracaoDao();
				List<ResultadoApuracao> lista=null;
				lista=rad.consultaResultadoEscola(cbEscola);
				atualizaTabela(lista);
			}
		});
		cbEscola.setVisible(false);
		cbEscola.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
				
			}
		});
		cbEscola.setBounds(155, 46, 217, 20);
		contentPane.add(cbEscola);
		
		JLabel lblFazerBuscaPor = new JLabel("Escolher tipo de busca");
		lblFazerBuscaPor.setBounds(39, 11, 202, 14);
		contentPane.add(lblFazerBuscaPor);
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdEscola);
		grupo.add(rdQuesito);
		
		cbQuesito = new JComboBox();
		cbQuesito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ResultadoApuracaoDao rad=new ResultadoApuracaoDao();
				List<ResultadoApuracao> lista=null;
				lista=rad.consultaResultadoQuesito(cbQuesito);
				atualizaTabela(lista);
			}
		});
		cbQuesito.setVisible(false);
		cbQuesito.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				
			}
		});
		cbQuesito.setBounds(610, 46, 202, 20);
		contentPane.add(cbQuesito);
	
	}
	
	
	
	public List<Quesito> listaQuesitos(){
		QuesitoDao pDao=new QuesitoDao();
		List<Quesito> lista=new ArrayList<Quesito>();
		lista=pDao.consultaListaQuesito();
		return lista;
	}
	
	
	public List<Escola> listaEscolas(){
		EscolaDao pDao=new EscolaDao();
		List<Escola> lista=new ArrayList<Escola>();
		lista=pDao.consultaListaEscola();
		return lista;
	}
	
	
	public void preencheComboQuesito(){
		if(cbQuesito.getItemCount()!=0){
			cbQuesito.removeAllItems();
		}
			
		List<Quesito> lista=listaQuesitos();
			if(lista!=null){
				for(Quesito p:lista){
					cbQuesito.addItem(p.getId_quesito()+"-"+p.getNome());
					
				}
			}
		
	}
	
	
	public void preencheComboEscola(){
		if(cbEscola.getItemCount()!=0){
			cbEscola.removeAllItems();
		}
		List<Escola> lista=listaEscolas();
			if(lista!=null){
				for(Escola p:lista){
					cbEscola.addItem(p.getId_escola()+"-"+p.getNome());
					
				}
			}
		
	}
	
	public void atualizaTabela(List<ResultadoApuracao> lista){
		
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(ResultadoApuracao p:lista){
				Object[] objeto=new Object[10];
				objeto[0]=p.getNomeEscola();
				objeto[1]=p.getNomeQuesito();
				objeto[2]=p.getNota1();
				objeto[3]=p.getNota2();
				objeto[4]=p.getNota3();
				objeto[5]=p.getNota4();
				objeto[6]=p.getNota5();
				objeto[7]=p.getNotaMaior();
				objeto[8]=p.getNotaMenor();
				objeto[9]=p.getNotaTotalQuesito();
				modelo.addRow(objeto);
				
			}
		}
		
	}
}



