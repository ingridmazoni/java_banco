package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import dao.*;
import pojo.*;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewApuracao extends JFrame {

	private JPanel contentPane;
	private JTextField txtNota;
	private JComboBox cbJurado;
	private JComboBox cbEscola;
	private JButton btnInserir;
	private JComboBox cbQuesito;
	private int q,e,j;
	private ActionListener opcaoApuracao;
	private TelaVerQuesito tvq;
	private TelaVerTotal tvt;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewApuracao frame = new ViewApuracao();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewApuracao() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				preencheComboQuesito();
				preencheComboJuradoQuesito();
				preencheComboEscola();
				tvq= new TelaVerQuesito();
				 tvt=new TelaVerTotal();
							
			}
		});
		setTitle("Apura\u00E7\u00E3o");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 327);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblQuesito = new JLabel("Quesito");
		lblQuesito.setBounds(25, 22, 46, 14);
		contentPane.add(lblQuesito);
		
		cbQuesito = new JComboBox();
		cbQuesito.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				preencheComboJuradoQuesito();
			}
		});
		cbQuesito.setBounds(73, 19, 235, 20);
		contentPane.add(cbQuesito);
		
		JLabel lblJurado = new JLabel("Jurado ");
		lblJurado.setBounds(25, 75, 46, 14);
		contentPane.add(lblJurado);
		
		cbJurado = new JComboBox();
		cbJurado.setBounds(73, 72, 235, 20);
		contentPane.add(cbJurado);
		
		JLabel lblEscola = new JLabel("Escola");
		lblEscola.setBounds(25, 132, 46, 14);
		contentPane.add(lblEscola);
		
		cbEscola = new JComboBox();
		cbEscola.setBounds(73, 129, 235, 20);
		contentPane.add(cbEscola);
		
		JLabel lblNota = new JLabel("Nota");
		lblNota.setBounds(25, 194, 46, 14);
		contentPane.add(lblNota);
		
		txtNota = new JTextField();
		txtNota.setBounds(72, 191, 86, 20);
		contentPane.add(txtNota);
		txtNota.setColumns(10);
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					insereNotas();
					mudaOpcaoCombos();
					
				}
				catch(SQLException erro){
					JOptionPane.showMessageDialog(null, erro.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException erro){
					JOptionPane.showMessageDialog(null, "O campo nota aceita apenas n�meros");
				}
				
			}
		});
		
				
		
		btnInserir.setBounds(174, 190, 89, 23);
		contentPane.add(btnInserir);
		
		JButton btnVerQuesito = new JButton("Ver Quesito");
		btnVerQuesito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tvq.show();
			}
		});
		btnVerQuesito.setBounds(127, 247, 123, 23);
		contentPane.add(btnVerQuesito);
		
		JButton btnVerTotal = new JButton("Ver Total");
		btnVerTotal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tvt.show();
			}
		});
		btnVerTotal.setBounds(260, 247, 89, 23);
		contentPane.add(btnVerTotal);
	}
	
	
	public List<Quesito> listaQuesitos(){
		QuesitoDao pDao=new QuesitoDao();
		List<Quesito> lista=new ArrayList<Quesito>();
		lista=pDao.consultaListaQuesito();
		return lista;
	}
	
	
	public List<Escola> listaEscolas(){
		EscolaDao pDao=new EscolaDao();
		List<Escola> lista=new ArrayList<Escola>();
		lista=pDao.consultaListaEscola();
		return lista;
	}
	
	
	public List<JuradoQuesito> listaJuradoQuesito(){
		JuradosQuesitoDao pDao=new JuradosQuesitoDao();
		List<JuradoQuesito> lista=new ArrayList<JuradoQuesito>();
		lista=pDao.consultaListaJuradoQuesito(cbQuesito);
		return lista;
	}
	
	
	public void preencheComboQuesito(){
		if(cbQuesito.getItemCount()!=0){
			cbQuesito.removeAllItems();
		}
		List<Quesito> lista=listaQuesitos();
			if(lista!=null){
				for(Quesito p:lista){
					cbQuesito.addItem(p.getId_quesito()+"-"+p.getNome());
					
				}
			}
		
	}
	
	
	public void preencheComboEscola(){
		if(cbEscola.getItemCount()!=0){
			cbEscola.removeAllItems();
		}
		List<Escola> lista=listaEscolas();
			if(lista!=null){
				for(Escola p:lista){
					cbEscola.addItem(p.getId_escola()+"-"+p.getNome());
					
				}
			}
		
	}
	
	
	public void preencheComboJuradoQuesito(){
		if(cbJurado.getItemCount()!=0){
			cbJurado.removeAllItems();
		}
		List<JuradoQuesito> lista=listaJuradoQuesito();
			if(lista!=null){
				for(JuradoQuesito p:lista){
					cbJurado.addItem(p.getId_juradoQuesito()+"-"+p.getNome());
					
				}
			}
		
	}
	
	
	public void insereNotas()throws SQLException,NumberFormatException{
		Apuracao ap=new Apuracao();
		
		String texto=cbQuesito.getSelectedItem().toString();
		String codigo=texto.substring(0,texto.lastIndexOf("-"));
		ap.setId_quesito(Integer.parseInt(codigo));
		texto="";
		codigo="";
		
		texto=cbJurado.getSelectedItem().toString();
		codigo=texto.substring(0,texto.lastIndexOf("-"));
		ap.setId_juradoQuesito(Integer.parseInt(codigo));
		texto="";
		codigo="";
		
		texto=cbEscola.getSelectedItem().toString();
		codigo=texto.substring(0,texto.lastIndexOf("-"));
		ap.setId_escola(Integer.parseInt(codigo));
			
		
			ap.setNota(Double.parseDouble(txtNota.getText()));
			
			ApuracaoDao apDao=new ApuracaoDao();
			
			apDao.insereNota(ap);
	}
	
	
	
	public void mudaOpcaoCombos(){
		e=cbEscola.getSelectedIndex();
		q=cbQuesito.getSelectedIndex();
		j=cbJurado.getSelectedIndex();
		
		if(e<(cbEscola.getItemCount()-1)){
			cbEscola.setSelectedIndex(e+1);
		}
		else{
			cbEscola.setSelectedIndex(0);

			if(j<(cbJurado.getItemCount()-1)){
				cbJurado.setSelectedIndex(j+1);
			}
			else{
				cbJurado.setSelectedIndex(0);
				
				if(q<(cbQuesito.getItemCount()-1)){
					cbQuesito.setSelectedIndex(q+1);
				}
				else{
					cbQuesito.setSelectedIndex(0);
						
				}
					
			}
			
						
		}
}
	

}
