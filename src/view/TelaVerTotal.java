package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import pojo.Escola;
import pojo.ResultadoApuracao;
import dao.EscolaDao;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TelaVerTotal extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel modelo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaVerTotal frame = new TelaVerTotal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaVerTotal() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				atualizaTabela();
			}
		});
		setTitle("Ver Total");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 357);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 26, 328, 257);
		contentPane.add(scrollPane);
		
		table = new JTable();
	
		modelo=new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Escola", "Nota Total "
			}
		);
		table.setModel(modelo);
		table.getColumnModel().getColumn(0).setPreferredWidth(285);
		table.getColumnModel().getColumn(1).setPreferredWidth(267);
		scrollPane.setViewportView(table);
	}
	
public void atualizaTabela(){
		
	List<Escola> lista=listaEscolas();
		
		if(modelo.getRowCount()!=0){
			modelo.setRowCount(0);
		}
		if(lista!=null){
			for(Escola p:lista){
				Object[] objeto=new Object[10];
				objeto[0]=p.getNome();
				objeto[1]=p.getTotalPontos();
				modelo.addRow(objeto);
				
			}
		}
		
	}

public List<Escola> listaEscolas(){
	EscolaDao pDao=new EscolaDao();
	List<Escola> lista=new ArrayList<Escola>();
	lista=pDao.consultaListaEscola();
	return lista;
}



	
}
