package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import dao.*;
import pojo.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroQuesitos extends JFrame {

	private JPanel contentPane;
	private JTextField txtCodigoQuesito;
	private JTextField txtNome;
	private JButton btnApagar;
	private JButton btnEditar ;
	private JButton btnInserir;
	private JMenu menuPesquisar;
	private JRadioButton rdPesquisar;
	private JRadioButton rdCadastrar;
	private JRadioButton rdAtualizar;
	private JRadioButton rdExcluir;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroQuesitos frame = new CadastroQuesitos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroQuesitos() {
		setTitle("Cadastro de Quesitos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 335);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuPesquisar = new JMenu("Pesquisar");
		menuBar.add(menuPesquisar);
		menuPesquisar.setVisible(false);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Por Nome");
		menuPesquisar.add(mntmNewMenuItem);
		
		JMenuItem mntmPorCdigoQuesito = new JMenuItem("Por C\u00F3digo");
		mntmPorCdigoQuesito.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Pesquisa();
			}
		});
		menuPesquisar.add(mntmPorCdigoQuesito);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCd = new JLabel("C\u00F3digo");
		lblCd.setBounds(34, 53, 90, 14);
		contentPane.add(lblCd);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(34, 99, 46, 14);
		contentPane.add(lblNome);
		
		
		txtCodigoQuesito = new JTextField();
		txtCodigoQuesito.setEditable(false);
		txtCodigoQuesito.setBounds(90, 50, 131, 20);
		contentPane.add(txtCodigoQuesito);
		txtCodigoQuesito.setColumns(10);
		
		txtNome = new JTextField();
		txtNome.setEditable(false);
		txtNome.setBounds(90, 96, 131, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		btnInserir = new JButton("Inserir");
		btnInserir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Inserir();
			
			}
		});
		btnInserir.setVisible(false);
		
		btnInserir.setBounds(23, 228, 89, 23);
		contentPane.add(btnInserir);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Atualiza();
				
			}
		});
		btnEditar.setVisible(false);
		
		btnEditar.setBounds(142, 228, 89, 23);
		contentPane.add(btnEditar);
		
		btnApagar = new JButton("Apagar");
		btnApagar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Excluir();
				
			}
		});
		btnApagar.setVisible(false);
		
		btnApagar.setBounds(253, 228, 89, 23);
		contentPane.add(btnApagar);
		
		
		
		
		rdPesquisar = new JRadioButton("Pesquisar");
		rdPesquisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilPesquisar();
			}
		});
		rdPesquisar.setBounds(286, 24, 109, 23);
		contentPane.add(rdPesquisar);
		
		rdCadastrar = new JRadioButton("Cadastrar");
		rdCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilCadastrar();
			}
		});
		rdCadastrar.setBounds(286, 70, 109, 23);
		contentPane.add(rdCadastrar);
		
		rdAtualizar = new JRadioButton("Atualizar");
		rdAtualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilEditar();
			}
		});
		rdAtualizar.setBounds(286, 119, 109, 23);
		contentPane.add(rdAtualizar);
		
		rdExcluir = new JRadioButton("Excluir");
		rdExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				perfilExcluir();
			}
		});
		rdExcluir.setBounds(286, 163, 109, 23);
		contentPane.add(rdExcluir);
		
		ButtonGroup grupo=new ButtonGroup();
		grupo.add(rdAtualizar);
		grupo.add(rdPesquisar);
		grupo.add(rdCadastrar);
		grupo.add(rdExcluir);
		
	}
	
	public void perfilCadastrar(){
		
		txtCodigoQuesito.setEditable(false);
		txtNome.setEditable(true);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(true);
		menuPesquisar.setVisible(false);
		
	}
	
	public void perfilExcluir(){
		txtCodigoQuesito.setEditable(false);
		txtNome.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(true);
		btnInserir.setVisible(false);
		menuPesquisar.setVisible(false);
	}
	
	public void perfilEditar(){
		txtCodigoQuesito.setEditable(false);
		txtNome.setEditable(true);
		btnEditar.setVisible(true);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		menuPesquisar.setVisible(false);
	}
	
	public void perfilPesquisar(){
		txtCodigoQuesito.setEditable(false);
		txtNome.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		menuPesquisar.setVisible(true);
	}
	
	public void Inserir(){
		Quesito quesito=new Quesito();
		quesito.setNome(txtNome.getText());
		
		QuesitoDao daoquesito=new QuesitoDao();
		daoquesito.insereQuesito(quesito);
		PerfilPadrao();
		
	}
	
	public void Excluir(){
		Quesito quesito=new Quesito();
		quesito.setId_quesito(Integer.parseInt(txtCodigoQuesito.getText()));
		
		QuesitoDao daoquesito=new QuesitoDao();
		daoquesito.excluiQuesito(quesito);
		PerfilPadrao();
		
	}
	
	public void Atualiza(){
		Quesito quesito=new Quesito();
		quesito.setId_quesito(Integer.parseInt(txtCodigoQuesito.getText()));
		quesito.setNome(txtNome.getText());
		
		QuesitoDao daoquesito=new QuesitoDao();
		daoquesito.atualizaQuesito(quesito);
		PerfilPadrao();
	}
	
	public void Pesquisa(){
		try{
			Quesito quesito=new Quesito();
			quesito.setId_quesito(Integer.parseInt(JOptionPane.showInputDialog("Digite o c�digo do quesito")));

			
			QuesitoDao daoquesito=new QuesitoDao();
			setQuesito(daoquesito.consultaQuesito(quesito));
			
			
		}
		catch(NumberFormatException erro){
			JOptionPane.showMessageDialog(null, "O c�digo digitado � invalido");
		}
		
	}
	
	
	public void setQuesito(Quesito quesito){
		txtCodigoQuesito.setText(String.valueOf(quesito.getId_quesito()));
		txtNome.setText(quesito.getNome());
		
	}
	
	public void PerfilPadrao(){
		txtCodigoQuesito.setText("");
		txtNome.setText("");
		txtCodigoQuesito.setEditable(false);
		txtNome.setEditable(false);
		btnEditar.setVisible(false);
		btnApagar.setVisible(false);
		btnInserir.setVisible(false);
		menuPesquisar.setVisible(false);
		
	}
}
