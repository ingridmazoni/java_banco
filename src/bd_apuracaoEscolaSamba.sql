create database bd_apuracaoEscolaSamba
use bd_apuracaoEscolaSamba


create table escola(
id_escola int identity(1,1) not null,
nome varchar(100),
totalPontos decimal(4,2)
primary key(id_escola)
)

create table quesito(
id_quesito int identity(1,1) not null,
nome varchar(100)
primary key(id_quesito)
)

create table jurado(
id_jurado int identity(1,1) not null,
nome varchar(100)
primary key(id_jurado)
)

create table juradosQuesito(
id_juradoQuesito int identity(1,1) not null,
id_jurado int not null,
id_quesito int not null,
unique(id_jurado,id_quesito),
numeroJurado int not null
primary key(id_juradoQuesito)
foreign key (id_jurado) references jurado(id_jurado),
foreign key (id_quesito) references quesito(id_quesito)
)



create table notas(
id_nota int identity(1,1) not null,
id_quesito int not null,
id_juradoQuesito int not null,
id_escola int not null,
unique(id_quesito,id_juradoQuesito,id_escola),
nota decimal(4,2)
primary key(id_nota)
foreign key(id_quesito) references quesito(id_quesito),
foreign key(id_juradoQuesito) references juradosQuesito(id_juradoQuesito),
foreign key(id_escola) references escola(id_escola)
)

create table resultado(
id_resultado int identity(1,1) not null,
id_escola int not null,
id_quesito int not null,
unique(id_escola,id_quesito),
nota1 decimal(4,2),
nota2 decimal(4,2),
nota3 decimal(4,2),
nota4 decimal(4,2),
nota5 decimal(4,2),
maiorNota decimal(4,2),
menorNota decimal(4,2),
notaTotalQuesito decimal(4,2)
primary key(id_resultado)
foreign key(id_escola) references escola(id_escola),
foreign key(id_quesito) references quesito(id_quesito),
)




insert into escola(nome) values('Acad�micos do Tatuap�')
insert into escola(nome) values('Rosas de Ouro')
insert into escola(nome) values('Mancha Verde')
insert into escola(nome) values('Vai Vai')
insert into escola(nome) values('X9 Paulistana')
insert into escola(nome) values('Drag�es da Real')
insert into escola(nome) values('�guia de Ouro')
insert into escola(nome) values('Nen� da Vila Matilde')
insert into escola(nome) values('Gavi�es da Fiel')
insert into escola(nome) values('Mocidade Alegre')
insert into escola(nome) values('Tom Maior')
insert into escola(nome) values('Unidos de Vila Maria')
insert into escola(nome) values('Acad�micos do Tucuruvi')
insert into escola(nome) values('Imp�rio de Casa Verde')

insert into quesito values('Comiss�o de Frente')
insert into quesito values('Evolu��o')
insert into quesito values('Fantasia')
insert into quesito values('Bateria')
insert into quesito values('Alegoria')
insert into quesito values('Harmonia')
insert into quesito values('Samba Enredo')
insert into quesito values('Mestre Sala e Porta Bandeira')
insert into quesito values('Enredo')
	
	--popula tabela jurados 
	declare @quant int
	set @quant=1
	while(@quant<=50)
	begin
		insert into jurado values('Jurado '+cast(@quant as varchar(100)))
		set @quant=@quant+1
	end
	
	
	--popula tabela jurados quesitos 
	
	declare @contador int
	set @contador=1
	declare @jur int
	set @jur=1
	declare @quesito int
	set @quesito=1
	
	while(@jur<=45)
	begin
		if(@contador>5)
		begin 
			set @contador=1
			set @quesito=@quesito+1
		end
	
		
		insert into juradosQuesito values(@jur,@quesito,@contador)
			set @contador=@contador+1
			set @jur=@jur+1
	end
	
	select * from juradosQuesito
	
	------------------------------------------------------------------------------------
	
	
insert into juradosQuesito values(44,5,1)
insert into juradosQuesito values(45,5,2)
insert into juradosQuesito values(46,5,3)
insert into juradosQuesito values(47,5,4)
insert into juradosQuesito values(48,5,5)
insert into juradosQuesito values(1,3,1)
insert into juradosQuesito values(2,3,2)
insert into juradosQuesito values(3,3,3)
insert into juradosQuesito values(4,3,4)
insert into juradosQuesito values(5,3,5)
insert into juradosQuesito values(44,4,1)
insert into juradosQuesito values(45,4,2)
insert into juradosQuesito values(46,4,3)
insert into juradosQuesito values(47,4,4)
insert into juradosQuesito values(48,4,5)

----------------------------------------------------------------------------
insert into notas values(5,1,2,10)
insert into notas values(5,2,2,9.5)
insert into notas values(5,3,2,7.5)	
insert into notas values(5,4,2,8.5)
insert into notas values(5,5,2,5.5)

----------------------------------------------------------------------------------------------		
		
create procedure insereNotas(@id_quesito int,@id_juradoQuesito int,@id_escola int,@nota decimal(4,2))
as
	declare @quantNotasQuesito int 
	set	 @quantNotasQuesito=(select COUNT(*) from notas where id_quesito=@id_quesito and id_escola=@id_escola)
	declare @verifica int
	set @verifica=(select juradosQuesito.id_quesito from juradosQuesito where id_juradoQuesito=@id_juradoQuesito)
	
		
		if(@quantNotasQuesito>=5)
			begin 
			raiserror ('Uma escola pode ter apenas 5 notas por quesito',16,1)
			end
		else
			begin 
				if(@id_quesito=@verifica)
				begin
						if(@nota>=5)and(@nota<=10)
						begin
							insert into notas values(@id_quesito,@id_juradoQuesito,@id_escola,@nota)
							exec alimentaResultado @id_quesito,@id_juradoQuesito,@id_escola,@nota 
						end
						else
						begin
							raiserror('A nota deve maior ou igual a 5 e menor ou igual a 10',16,1)
						end
				end
				else
					begin
						raiserror('O jurado selecionado n�o est� cadastrado como julgador deste quesito ',16,1)
					
					end	
			end
--------------------------------------------------------------------------------------------------------------------			
			
		exec insereNotas 5,21,2,10.00
		exec insereNotas 5,22,2,9.5
		exec insereNotas 5,23,2,7.5	
		exec insereNotas 5,24,2,8.5
		exec insereNotas 5,25,2,6.5
					
		exec insereNotas 2,6,2,10.00
		exec insereNotas 2,7,2,9.5
		exec insereNotas 2,8,2,7.5	
		exec insereNotas 2,9,2,8.5
		exec insereNotas 4,18,3,2.5
		
		delete from notas
		delete from resultado
		
		select * from juradosQuesito
		
		select * from escola
		select * from notas
		select * from resultado
		
		exec insereNotas 2,6,2,8.3
		
		delete from notas
		delete from resultado
----------------------------------------------------------------------------------------------------------------------		
		select * from notas
		select * from juradosQuesito
		select * from escola
		select * from resultado
		
		
		delete from notas
		delete from resultado

-----------------------------------------------------------------------------------------------------------------

		create procedure listaJuradosQuesito(@id_quesito int)
		as
			
		select juradosQuesito.id_juradoQuesito,juradosQuesito.id_jurado,juradosQuesito.id_quesito,juradosQuesito.numeroJurado,jurado.nome as nome 
		from juradosQuesito inner join jurado on
		jurado.id_jurado=juradosQuesito.id_jurado where id_quesito=@id_quesito
		

-------------------------------------------------------------------------------------------------------------------		
		
		create procedure insereJuradosQuesitos(@id_jurado int,@id_quesito int)
		as
		
		declare @juradosQuesitos int
		set @juradosQuesitos=(select COUNT(*) from juradosQuesito where id_quesito=@id_quesito)
		
		if(@juradosQuesitos>=5)
		begin
			raiserror ('Um quesito pode ter apenas 5 jurados cadastrados nele',16,1)
		
		end
		else
		begin 
			insert into juradosQuesito values(@id_jurado,@id_quesito,@juradosQuesitos+1)
		end
		
----------------------------------------------------------------------------------------------------		
		
		create procedure listaJuradosQuesito2(@id_jurado int)
		as
			
		select juradosQuesito.id_jurado,juradosQuesito.id_quesito,quesito.nome,juradosQuesito.numeroJurado 
		from juradosQuesito inner join quesito on
		quesito.id_quesito=juradosQuesito.id_quesito where id_jurado=@id_jurado
		
---------------------------------------------------------------------------------------
	
	create procedure alimentaResultado(@id_quesito int,@id_juradoQuesito int,@id_escola int,@nota decimal(4,2))
	as
	declare @nota1 decimal(4,2)
			declare @nota2 decimal(4,2)
			declare @nota3 decimal(4,2)
			declare @nota4 decimal(4,2)
			declare @nota5 decimal(4,2)
			set @nota1=0
			set @nota2=0
			set @nota3=0
			set @nota4=0
			set @nota5=0
					
			declare @cont int
			set @cont=1
						
			while(@cont<=5)
				begin
			
			declare @temp decimal(4,2)
			
			set @temp=(select notas.nota from notas inner join juradosQuesito on
			notas.id_juradoQuesito=juradosQuesito.id_juradoQuesito
			where notas.id_quesito=@id_quesito and notas.id_escola=@id_escola and 
			juradosQuesito.numeroJurado=@cont)
			
				if(@cont=1 and @temp is not null)
					begin
						set @nota1=@temp
					end
				if(@cont=2 and @temp is not null)
					begin
						set @nota2=@temp
					end	
				if(@cont=3 and @temp is not null)
					begin
						set @nota3=@temp
					end
				if(@cont=4 and @temp is not null)
					begin
						set @nota4=@temp
					end
				if(@cont=5 and @temp is not null)
					begin
						set @nota5=@temp
					end
				set @cont=@cont+1					
				
				end
				
				declare @notaMaior decimal(4,2)
				declare @notaMenor decimal(4,2)
				declare @notaTotalQuesito decimal(4,2)
				declare @cont2 int
				set @notaMaior=(select MAX(notas.nota) from notas where notas.id_quesito=@id_quesito and notas.id_escola=@id_escola)
				set @notaMenor=(select MIN(notas.nota) from notas where notas.id_quesito=@id_quesito and notas.id_escola=@id_escola)
				set @notaTotalQuesito=((select SUM(notas.nota) from notas where notas.id_quesito=@id_quesito and notas.id_escola=@id_escola)-(@notaMaior+@notaMenor))
				
				set @cont=(select count(*) from resultado where id_escola=@id_escola and id_quesito=@id_quesito)
				set @cont2=(select COUNT(*) from notas where id_escola=@id_escola and id_quesito=@id_quesito) 
				
				if(@cont=0)
				begin
					insert into resultado(id_escola,id_quesito,nota1,nota2,nota3,nota4,nota5) values(@id_escola,@id_quesito,@nota1,@nota2,@nota3,@nota4,@nota5)
				end
				else
				begin
					if(@cont2>=3)
					begin
						update resultado set nota1=@nota1,nota2=@nota2,nota3=@nota3,nota4=@nota4,nota5=@nota5,maiorNota=@notaMaior,menorNota=@notaMenor,notaTotalQuesito=@notaTotalQuesito 
						where id_escola=@id_escola and id_quesito=@id_quesito
						
						declare @totalPontos int
						set @totalPontos=(select SUM(notaTotalQuesito) from resultado where id_escola=@id_escola)
						
						update escola set totalPontos=@totalPontos where id_escola=@id_escola
						 
						
					end
					else
					begin
						update resultado set nota1=@nota1,nota2=@nota2,nota3=@nota3,nota4=@nota4,nota5=@nota5 where id_escola=@id_escola and id_quesito=@id_quesito
					
					end
				
				end	
				
	----------------------------------------------------------------------------------------------------
	
				
		create procedure resultado1(@id_quesito int) as
		select escola.nome as nomeEscola,quesito.nome as nomeQuesito,resultado.nota1,resultado.nota2,resultado.nota3,
		resultado.nota4,resultado.nota5,resultado.maiorNota,resultado.menorNota,resultado.notaTotalQuesito
		from escola inner join resultado on	escola.id_escola=resultado.id_escola
		inner join quesito on resultado.id_quesito=quesito.id_quesito where resultado.id_quesito=@id_quesito
		order by resultado.notaTotalQuesito desc
		
-------------------------------------------------------------------------------------------------------------------
		create procedure resultado2(@id_escola int) as
		select escola.nome as nomeEscola,quesito.nome as nomeQuesito,resultado.nota1,resultado.nota2,resultado.nota3,
		resultado.nota4,resultado.nota5,resultado.maiorNota,resultado.menorNota,resultado.notaTotalQuesito
		from escola inner join resultado on	escola.id_escola=resultado.id_escola
		inner join quesito on resultado.id_quesito=quesito.id_quesito where resultado.id_escola=@id_escola
		order by resultado.notaTotalQuesito desc 
		
		
------------------------------------------------------------------------------------------------------------

		