package pojo;
import view.*;
import dao.*;

public class JuradoQuesito{
	int id_juradoQuesito;
	int id_jurado;
	int id_quesito;
	int numeroJurado;
	String nome;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getId_juradoQuesito() {
		return id_juradoQuesito;
	}
	public void setId_juradoQuesito(int id_juradoQuesito) {
		this.id_juradoQuesito = id_juradoQuesito;
	}
	public int getId_jurado() {
		return id_jurado;
	}
	public void setId_jurado(int id_jurado) {
		this.id_jurado = id_jurado;
	}
	public int getId_quesito() {
		return id_quesito;
	}
	public void setId_quesito(int id_quesito) {
		this.id_quesito = id_quesito;
	}
	public int getNumeroJurado() {
		return numeroJurado;
	}
	public void setNumeroJurado(int numeroJurado) {
		this.numeroJurado = numeroJurado;
	}
	
	

}
