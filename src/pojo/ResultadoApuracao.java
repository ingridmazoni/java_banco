package pojo;

public class ResultadoApuracao {
	String nomeEscola;
	String nomeQuesito;
	double nota1;
	double nota2;
	double nota3;
	double nota4;
	double nota5;
	double notaMaior;
	double notaMenor;
	double notaTotalQuesito;
	
	
	public String getNomeEscola() {
		return nomeEscola;
	}
	public void setNomeEscola(String nomeEscola) {
		this.nomeEscola = nomeEscola;
	}
	public String getNomeQuesito() {
		return nomeQuesito;
	}
	public void setNomeQuesito(String nomeQuesito) {
		this.nomeQuesito = nomeQuesito;
	}
	public double getNota1() {
		return nota1;
	}
	public void setNota1(double nota1) {
		this.nota1 = nota1;
	}
	public double getNota2() {
		return nota2;
	}
	public void setNota2(double nota2) {
		this.nota2 = nota2;
	}
	public double getNota3() {
		return nota3;
	}
	public void setNota3(double nota3) {
		this.nota3 = nota3;
	}
	public double getNota4() {
		return nota4;
	}
	public void setNota4(double nota4) {
		this.nota4 = nota4;
	}
	public double getNota5() {
		return nota5;
	}
	public void setNota5(double nota5) {
		this.nota5 = nota5;
	}
	public double getNotaMaior() {
		return notaMaior;
	}
	public void setNotaMaior(double notaMaior) {
		this.notaMaior = notaMaior;
	}
	public double getNotaMenor() {
		return notaMenor;
	}
	public void setNotaMenor(double notaMenor) {
		this.notaMenor = notaMenor;
	}
	public double getNotaTotalQuesito() {
		return notaTotalQuesito;
	}
	public void setNotaTotalQuesito(double notaTotalQuesito) {
		this.notaTotalQuesito = notaTotalQuesito;
	}
	
	
	

}
