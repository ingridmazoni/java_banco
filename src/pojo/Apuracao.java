package pojo;

public class Apuracao {
	private int id_quesito;
	private int id_juradoQuesito;
	private int id_escola;
	private double nota;
	
	public int getId_quesito() {
		return id_quesito;
	}
	public void setId_quesito(int id_quesito) {
		this.id_quesito = id_quesito;
	}
	public int getId_juradoQuesito() {
		return id_juradoQuesito;
	}
	public void setId_juradoQuesito(int id_juradoQuesito) {
		this.id_juradoQuesito = id_juradoQuesito;
	}
	public int getId_escola() {
		return id_escola;
	}
	public void setId_escola(int id_escola) {
		this.id_escola = id_escola;
	}
	public double getNota() {
		return nota;
	}
	public void setNota(double nota) {
		this.nota = nota;
	}
	
	

}
