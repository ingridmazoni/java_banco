package pojo;
import view.*;
import dao.*;

public class Notas {
	int id_nota;
	int id_quesito;
	int id_juradoQuesito;
	int id_escola;
	int nota;
	
	public int getId_nota() {
		return id_nota;
	}
	public void setId_nota(int id_nota) {
		this.id_nota = id_nota;
	}
	public int getId_quesito() {
		return id_quesito;
	}
	public void setId_quesito(int id_quesito) {
		this.id_quesito = id_quesito;
	}
	public int getId_juradoQuesito() {
		return id_juradoQuesito;
	}
	public void setId_juradoQuesito(int id_juradoQuesito) {
		this.id_juradoQuesito = id_juradoQuesito;
	}
	public int getId_escola() {
		return id_escola;
	}
	public void setId_escola(int id_escola) {
		this.id_escola = id_escola;
	}
	public int getNota() {
		return nota;
	}
	public void setNota(int nota) {
		this.nota = nota;
	}
	
	
}
