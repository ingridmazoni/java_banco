package pojo;
import view.*;
import dao.*;

public class Escola {
	int id_escola ;
	String nome;
	Double totalPontos;
	
	public int getId_escola() {
		return id_escola;
	}
	public void setId_escola(int id_escola) {
		this.id_escola = id_escola;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getTotalPontos() {
		return totalPontos;
	}
	public void setTotalPontos(Double totalPontos) {
		this.totalPontos = totalPontos;
	}
	
	
	
	

}
