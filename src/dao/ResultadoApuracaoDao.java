package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import pojo.*;

public class ResultadoApuracaoDao {
	
Connection c;
	
	public ResultadoApuracaoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public List<ResultadoApuracao> consultaResultadoEscola(JComboBox cbEscolha){
		List<ResultadoApuracao> listaResultadoApuracao=new ArrayList<ResultadoApuracao>();
		
		String sql="{call resultado2(?)}";
		String texto=cbEscolha.getSelectedItem().toString();
		String codigo=texto.substring(0,texto.lastIndexOf("-"));
		
		
		 try {
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1,Integer.parseInt(codigo));
			ResultSet rs=cs.executeQuery();
			
			while(rs.next()){
				ResultadoApuracao ResultadoApuracaoConsultado=new ResultadoApuracao();
				ResultadoApuracaoConsultado.setNomeEscola(rs.getString("nomeEscola"));
				ResultadoApuracaoConsultado.setNomeQuesito(rs.getString("nomeQuesito"));
				ResultadoApuracaoConsultado.setNota1(rs.getDouble("nota1"));
				ResultadoApuracaoConsultado.setNota2(rs.getDouble("nota2"));
				ResultadoApuracaoConsultado.setNota3(rs.getDouble("nota3"));
				ResultadoApuracaoConsultado.setNota4(rs.getDouble("nota4"));
				ResultadoApuracaoConsultado.setNota5(rs.getDouble("nota5"));
				ResultadoApuracaoConsultado.setNotaMaior(rs.getDouble("maiorNota"));
				ResultadoApuracaoConsultado.setNotaMenor(rs.getDouble("menorNota"));
				ResultadoApuracaoConsultado.setNotaTotalQuesito(rs.getDouble("notaTotalQuesito"));
				
				listaResultadoApuracao.add(ResultadoApuracaoConsultado);
			}
			
			rs.close();
			cs.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaResultadoApuracao;
		 

}

	
	public List<ResultadoApuracao> consultaResultadoQuesito(JComboBox cbEscolha){
		List<ResultadoApuracao> listaResultadoApuracao=new ArrayList<ResultadoApuracao>();
		
		String sql="{call resultado1(?)}";
		String texto=cbEscolha.getSelectedItem().toString();
		String codigo=texto.substring(0,texto.lastIndexOf("-"));
		
		
		 try {
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1,Integer.parseInt(codigo));
			ResultSet rs=cs.executeQuery();
			
			while(rs.next()){
				ResultadoApuracao ResultadoApuracaoConsultado=new ResultadoApuracao();
				ResultadoApuracaoConsultado.setNomeEscola(rs.getString("nomeEscola"));
				ResultadoApuracaoConsultado.setNomeQuesito(rs.getString("nomeQuesito"));
				ResultadoApuracaoConsultado.setNota1(rs.getDouble("nota1"));
				ResultadoApuracaoConsultado.setNota2(rs.getDouble("nota2"));
				ResultadoApuracaoConsultado.setNota3(rs.getDouble("nota3"));
				ResultadoApuracaoConsultado.setNota4(rs.getDouble("nota4"));
				ResultadoApuracaoConsultado.setNota5(rs.getDouble("nota5"));
				ResultadoApuracaoConsultado.setNotaMaior(rs.getDouble("maiorNota"));
				ResultadoApuracaoConsultado.setNotaMenor(rs.getDouble("menorNota"));
				ResultadoApuracaoConsultado.setNotaTotalQuesito(rs.getDouble("notaTotalQuesito"));
				
				listaResultadoApuracao.add(ResultadoApuracaoConsultado);
			}
			
			rs.close();
			cs.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaResultadoApuracao;
		 

}
	
	
}
	