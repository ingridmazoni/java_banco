package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import pojo.*;
import view.*;


public class QuesitoDao {
	
	Connection c;
	
	public QuesitoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereQuesito(Quesito quesito){
		boolean inserido=false;
		String sql="INSERT INTO quesito VALUES(?)";
		try{
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, quesito.getNome());
			ps.execute();
			ps.close();
			inserido=true;
		}
		catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		return inserido;
		
	}
	
	public boolean atualizaQuesito(Quesito quesito){
		boolean atualizado=false;
		String sql="UPDATE quesito set nome=? where id_quesito=?";
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, quesito.getNome());
			ps.setInt(2, quesito.getId_quesito());
			ps.execute();
			ps.close();
			atualizado=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		
		}
		return atualizado;
		
	}
	
	public boolean excluiQuesito(Quesito quesito){
		boolean excluido=false;
		String sql="DELETE from quesito where id_quesito=?";
		
		PreparedStatement ps;
		try {
			ps = c.prepareStatement(sql);
			ps.setInt(1, quesito.getId_quesito());
			ps.execute();
			ps.close();
			excluido=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		
		return excluido;
	}
	
	public Quesito consultaQuesito(Quesito quesito){
		Quesito QuesitoConsultado=new Quesito();
		String sql="SELECT id_quesito,nome from quesito where id_quesito=?";
		
		
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setInt(1, quesito.getId_quesito());
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				QuesitoConsultado.setId_quesito(rs.getInt("id_quesito"));
				QuesitoConsultado.setNome(rs.getString("nome"));
						
			}
			rs.close();
			ps.close();
			
			 
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		return QuesitoConsultado;
		
	}
	
	public List<Quesito> consultaListaQuesito(){
		List<Quesito> listaQuesito=new ArrayList<Quesito>();
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT * from quesito");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Quesito QuesitoConsultado=new Quesito();
				QuesitoConsultado.setId_quesito(rs.getInt("id_quesito"));
				QuesitoConsultado.setNome(rs.getString("nome"));
			
				listaQuesito.add(QuesitoConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaQuesito;
		 
		
		
	}
	
	


}
