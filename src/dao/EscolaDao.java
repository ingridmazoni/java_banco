package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.Escola;

public class EscolaDao {
Connection c;
	
	public EscolaDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereEscola(Escola escola){
		boolean inserido=false;
		String sql="INSERT INTO escola(nome) VALUES(?)";
		try{
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, escola.getNome());
			System.out.println(escola.getNome());
			ps.execute();
			ps.close();
			inserido=true;
		}
		catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		return inserido;
		
	}
	
	public boolean atualizaEscola(Escola escola){
		boolean atualizado=false;
		String sql="UPDATE escola set nome=? where id_escola=?";
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, escola.getNome());
			ps.setInt(2, escola.getId_escola());
			ps.execute();
			ps.close();
			atualizado=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		
		}
		return atualizado;
		
	}
	
	public boolean excluiEscola(Escola escola){
		boolean excluido=false;
		String sql="DELETE from escola where id_escola=?";
		
		PreparedStatement ps;
		try {
			ps = c.prepareStatement(sql);
			ps.setInt(1, escola.getId_escola());
			ps.execute();
			ps.close();
			excluido=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		
		return excluido;
	}
	
	public Escola consultaEscola(Escola escola){
		Escola EscolaConsultado=new Escola();
		String sql="SELECT id_escola,nome from escola where id_escola=?";
		
		
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setInt(1, escola.getId_escola());
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				EscolaConsultado.setId_escola(rs.getInt("id_escola"));
				EscolaConsultado.setNome(rs.getString("nome"));
						
			}
			rs.close();
			ps.close();
			
			 
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		return EscolaConsultado;
		
	}
	
	public List<Escola> consultaListaEscola(){
		List<Escola> listaEscola=new ArrayList<Escola>();
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT * from escola order by totalPontos");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Escola EscolaConsultado=new Escola();
				EscolaConsultado.setId_escola(rs.getInt("id_escola"));
				EscolaConsultado.setNome(rs.getString("nome"));
				EscolaConsultado.setTotalPontos(rs.getDouble("totalPontos"));
				listaEscola.add(EscolaConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaEscola;
		 
		
		
	}
	
	


}
