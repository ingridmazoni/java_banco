package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import pojo.*;
import pojo.Apuracao;
import view.*;

public class ApuracaoDao {
Connection c;
	
	public ApuracaoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereNota(Apuracao apuracao)throws SQLException{
		boolean inserido=false;
		String sql="{call insereNotas(?,?,?,?)}";
		
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1, apuracao.getId_quesito());
			cs.setInt(2, apuracao.getId_juradoQuesito());
			cs.setInt(3, apuracao.getId_escola());
			cs.setDouble(4, apuracao.getNota());
			cs.execute();
			cs.close();
			inserido=true;
		
			
			
			
		
		return inserido;
		
	}

}
