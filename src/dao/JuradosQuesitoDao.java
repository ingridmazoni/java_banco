package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import pojo.JuradoQuesito;

public class JuradosQuesitoDao {
Connection c;
	
	public JuradosQuesitoDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereJuradoQuesito(JuradoQuesito juradoQuesito){
		boolean inserido=false;
		String sql="{call insereJuradosQuesitos(?,?)}";
		try{
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1, juradoQuesito.getId_jurado());
			cs.setInt(2, juradoQuesito.getId_quesito());
			cs.execute();
			cs.close();
			inserido=true;
		}
		catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		return inserido;
		
	}
	
		
	public boolean excluiJuradoQuesito(JuradoQuesito juradoQuesito){
		boolean excluido=false;
		String sql="DELETE from juradosQuesito where id_jurado=?";
		
		PreparedStatement ps;
		try {
			ps = c.prepareStatement(sql);
			ps.setInt(1, juradoQuesito.getId_juradoQuesito());
			ps.execute();
			ps.close();
			excluido=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		
		return excluido;
	}
	
	
	
	public List<JuradoQuesito> consultaListaJuradoQuesito(JComboBox cbquesito){
		List<JuradoQuesito> listaJuradoQuesito=new ArrayList<JuradoQuesito>();
		
		String sql="{call listaJuradosQuesito(?)}";
		String texto=cbquesito.getSelectedItem().toString();
		String codigo=texto.substring(0,texto.lastIndexOf("-"));
		
		
		 try {
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1,Integer.parseInt(codigo));
			ResultSet rs=cs.executeQuery();
			
			while(rs.next()){
				JuradoQuesito JuradoQuesitoConsultado=new JuradoQuesito();
				JuradoQuesitoConsultado.setId_juradoQuesito(rs.getInt("id_juradoQuesito"));
				JuradoQuesitoConsultado.setId_jurado(rs.getInt("id_jurado"));
				JuradoQuesitoConsultado.setId_quesito(rs.getInt("id_quesito"));
				JuradoQuesitoConsultado.setNumeroJurado(rs.getInt("numeroJurado"));
				JuradoQuesitoConsultado.setNome(rs.getString("nome"));
			
				listaJuradoQuesito.add(JuradoQuesitoConsultado);
			}
			
			rs.close();
			cs.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaJuradoQuesito;
		 
		
		
	}
	
	
	
	public List<JuradoQuesito> consultaListaJuradoQuesito2(JuradoQuesito juradoQuesito){
		List<JuradoQuesito> listaJuradoQuesito=new ArrayList<JuradoQuesito>();
		
		String sql="{call listaJuradosQuesito2(?)}";
		
		
		 try {
			CallableStatement cs=c.prepareCall(sql);
			cs.setInt(1,juradoQuesito.getId_jurado());
			ResultSet rs=cs.executeQuery();
			
			while(rs.next()){
				JuradoQuesito JuradoQuesitoConsultado=new JuradoQuesito();
				JuradoQuesitoConsultado.setId_jurado(rs.getInt("id_jurado"));
				JuradoQuesitoConsultado.setId_quesito(rs.getInt("id_quesito"));
				JuradoQuesitoConsultado.setNome(rs.getString("nome"));
				JuradoQuesitoConsultado.setNumeroJurado(rs.getInt("numeroJurado"));
				
			
				listaJuradoQuesito.add(JuradoQuesitoConsultado);
			}
			
			rs.close();
			cs.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaJuradoQuesito;
		 
		
		
	}
	
	
	
	
}
