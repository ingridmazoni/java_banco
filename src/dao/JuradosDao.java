package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import pojo.Jurado;

public class JuradosDao {
Connection c;
	
	public JuradosDao(){
		GenericDao gDao=new GenericDao();
		c=gDao.getConnection();
	}
	
	public boolean insereJurado(Jurado jurado){
		boolean inserido=false;
		String sql="INSERT INTO jurado VALUES(?)";
		try{
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, jurado.getNome());
			ps.execute();
			ps.close();
			inserido=true;
		}
		catch (SQLException e) {
			
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		}
		return inserido;
		
	}
	
	public boolean atualizaJurado(Jurado jurado){
		boolean atualizado=false;
		String sql="UPDATE jurado set nome=? where id_jurado=?";
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setString(1, jurado.getNome());
			ps.setInt(2, jurado.getId_jurado());
			ps.execute();
			ps.close();
			atualizado=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
			
		
		}
		return atualizado;
		
	}
	
	public boolean excluiJurado(Jurado jurado){
		boolean excluido=false;
		String sql="DELETE from jurado where id_jurado=?";
		
		PreparedStatement ps;
		try {
			ps = c.prepareStatement(sql);
			ps.setInt(1, jurado.getId_jurado());
			ps.execute();
			ps.close();
			excluido=true;
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		
		return excluido;
	}
	
	public Jurado consultaJurado(Jurado jurado){
		Jurado JuradoConsultado=new Jurado();
		String sql="SELECT id_jurado,nome from jurado where id_jurado=?";
		
		
		try {
			PreparedStatement ps=c.prepareStatement(sql);
			ps.setInt(1, jurado.getId_jurado());
			ResultSet rs=ps.executeQuery();
			
			if(rs.next()){
				JuradoConsultado.setId_jurado(rs.getInt("id_jurado"));
				JuradoConsultado.setNome(rs.getString("nome"));
						
			}
			rs.close();
			ps.close();
			
			 
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
		
		return JuradoConsultado;
		
	}
	
	public List<Jurado> consultaListaJurado(){
		List<Jurado> listaJurado=new ArrayList<Jurado>();
		StringBuffer sql=new StringBuffer();
		sql.append("SELECT * from jurado");
		
		
		 try {
			PreparedStatement ps=c.prepareStatement(sql.toString());
			ResultSet rs=ps.executeQuery();
			
			while(rs.next()){
				Jurado JuradoConsultado=new Jurado();
				JuradoConsultado.setId_jurado(rs.getInt("id_jurado"));
				JuradoConsultado.setNome(rs.getString("nome"));
			
				listaJurado.add(JuradoConsultado);
			}
			
			rs.close();
			ps.close();
			
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
		}
			
		 return listaJurado;
		 
		
		
	}
	
	


}
